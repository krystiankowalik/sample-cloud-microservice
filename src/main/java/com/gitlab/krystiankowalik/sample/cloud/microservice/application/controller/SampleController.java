package com.gitlab.krystiankowalik.sample.cloud.microservice.application.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/sample")
@RefreshScope
public class SampleController {

    @Value("${com.gitlab.krystiankowalik.sample.cloud.microservice.test-property:default-test-property-value}")
    private String testProperty;

    @Value("${com.gitlab.krystiankowalik.sample.cloud.microservice.test-property}")
    private String customProperty;
    @Value("${some.local.prop:defaultValue}")
    private String customProperty2;
    @Value("${encrypted.property}")
    private String encryptedProperty;

    @PostConstruct
    public void post() {
        System.out.println(customProperty);
        System.out.println(customProperty2);
        System.out.println(encryptedProperty);
    }

    @GetMapping("/hello")
    public String hello() {
        return this.testProperty;
    }
}
